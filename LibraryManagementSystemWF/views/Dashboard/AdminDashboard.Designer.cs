﻿namespace LibraryManagementSystemWF.Dashboard
{
    partial class AdminDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel1 = new Panel();
            btnMembers = new Button();
            btnBooks = new Button();
            btnLoans = new Button();
            btnStatus = new Button();
            btnAuthor = new Button();
            panel4 = new Panel();
            btnDashboard = new Button();
            panel2 = new Panel();
            label2 = new Label();
            label1 = new Label();
            panel3 = new Panel();
            label3 = new Label();
            panelMainDesktop = new Panel();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            panel3.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.BackColor = Color.DimGray;
            panel1.Controls.Add(btnMembers);
            panel1.Controls.Add(btnBooks);
            panel1.Controls.Add(btnLoans);
            panel1.Controls.Add(btnStatus);
            panel1.Controls.Add(btnAuthor);
            panel1.Controls.Add(panel4);
            panel1.Controls.Add(btnDashboard);
            panel1.Controls.Add(panel2);
            panel1.Location = new Point(0, -2);
            panel1.Name = "panel1";
            panel1.Size = new Size(166, 453);
            panel1.TabIndex = 0;
            // 
            // btnMembers
            // 
            btnMembers.BackColor = Color.DimGray;
            btnMembers.FlatAppearance.BorderSize = 0;
            btnMembers.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 255, 255);
            btnMembers.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 255, 255);
            btnMembers.FlatStyle = FlatStyle.Flat;
            btnMembers.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnMembers.ForeColor = SystemColors.ActiveCaptionText;
            btnMembers.Location = new Point(0, 292);
            btnMembers.Name = "btnMembers";
            btnMembers.Size = new Size(166, 40);
            btnMembers.TabIndex = 5;
            btnMembers.Text = "MEMBERS";
            btnMembers.UseVisualStyleBackColor = false;
            btnMembers.Click += btnMembers_Click;
            // 
            // btnBooks
            // 
            btnBooks.BackColor = Color.DimGray;
            btnBooks.FlatAppearance.BorderSize = 0;
            btnBooks.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 255, 255);
            btnBooks.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 255, 255);
            btnBooks.FlatStyle = FlatStyle.Flat;
            btnBooks.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnBooks.ForeColor = SystemColors.ActiveCaptionText;
            btnBooks.Location = new Point(0, 218);
            btnBooks.Name = "btnBooks";
            btnBooks.Size = new Size(166, 40);
            btnBooks.TabIndex = 5;
            btnBooks.Text = "BOOKS";
            btnBooks.UseVisualStyleBackColor = false;
            btnBooks.Click += btnBooks_Click;
            // 
            // btnLoans
            // 
            btnLoans.BackColor = Color.DimGray;
            btnLoans.FlatAppearance.BorderSize = 0;
            btnLoans.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 255, 255);
            btnLoans.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 255, 255);
            btnLoans.FlatStyle = FlatStyle.Flat;
            btnLoans.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnLoans.ForeColor = SystemColors.ActiveCaptionText;
            btnLoans.Location = new Point(0, 255);
            btnLoans.Name = "btnLoans";
            btnLoans.Size = new Size(166, 40);
            btnLoans.TabIndex = 4;
            btnLoans.Text = "LOANS";
            btnLoans.UseVisualStyleBackColor = false;
            btnLoans.Click += btnLoans_Click;
            // 
            // btnStatus
            // 
            btnStatus.BackColor = Color.DimGray;
            btnStatus.FlatAppearance.BorderSize = 0;
            btnStatus.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 255, 255);
            btnStatus.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 255, 255);
            btnStatus.FlatStyle = FlatStyle.Flat;
            btnStatus.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnStatus.ForeColor = SystemColors.ActiveCaptionText;
            btnStatus.Location = new Point(0, 143);
            btnStatus.Name = "btnStatus";
            btnStatus.Size = new Size(166, 40);
            btnStatus.TabIndex = 5;
            btnStatus.Text = "STATUS";
            btnStatus.UseVisualStyleBackColor = false;
            btnStatus.Click += btnStatus_Click;
            // 
            // btnAuthor
            // 
            btnAuthor.BackColor = Color.DimGray;
            btnAuthor.FlatAppearance.BorderSize = 0;
            btnAuthor.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 255, 255);
            btnAuthor.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 255, 255);
            btnAuthor.FlatStyle = FlatStyle.Flat;
            btnAuthor.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnAuthor.ForeColor = SystemColors.ActiveCaptionText;
            btnAuthor.Location = new Point(0, 181);
            btnAuthor.Name = "btnAuthor";
            btnAuthor.Size = new Size(166, 40);
            btnAuthor.TabIndex = 4;
            btnAuthor.Text = "AUTHOR";
            btnAuthor.UseVisualStyleBackColor = false;
            btnAuthor.Click += btnAuthor_Click;
            // 
            // panel4
            // 
            panel4.BackColor = Color.DimGray;
            panel4.Location = new Point(0, 71);
            panel4.Name = "panel4";
            panel4.Size = new Size(166, 39);
            panel4.TabIndex = 4;
            // 
            // btnDashboard
            // 
            btnDashboard.BackColor = Color.DimGray;
            btnDashboard.FlatAppearance.BorderSize = 0;
            btnDashboard.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 255, 255);
            btnDashboard.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 255, 255);
            btnDashboard.FlatStyle = FlatStyle.Flat;
            btnDashboard.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            btnDashboard.ForeColor = SystemColors.ActiveCaptionText;
            btnDashboard.Location = new Point(0, 106);
            btnDashboard.Name = "btnDashboard";
            btnDashboard.Size = new Size(166, 40);
            btnDashboard.TabIndex = 4;
            btnDashboard.Text = "DASHBOARD";
            btnDashboard.UseVisualStyleBackColor = false;
            btnDashboard.Click += btnDashboard_Click;
            // 
            // panel2
            // 
            panel2.BackColor = Color.DimGray;
            panel2.Controls.Add(label2);
            panel2.Controls.Add(label1);
            panel2.Location = new Point(0, 3);
            panel2.Name = "panel2";
            panel2.Size = new Size(163, 69);
            panel2.TabIndex = 1;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Arial Narrow", 10F, FontStyle.Regular, GraphicsUnit.Point);
            label2.ForeColor = SystemColors.Control;
            label2.Location = new Point(58, 28);
            label2.Name = "label2";
            label2.Size = new Size(94, 17);
            label2.TabIndex = 2;
            label2.Text = "MANAGEMENT";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Arial Narrow", 10F, FontStyle.Regular, GraphicsUnit.Point);
            label1.ForeColor = SystemColors.Control;
            label1.Location = new Point(58, 11);
            label1.Name = "label1";
            label1.Size = new Size(57, 17);
            label1.TabIndex = 1;
            label1.Text = "LIBRARY";
            // 
            // panel3
            // 
            panel3.BackColor = Color.DimGray;
            panel3.Controls.Add(label3);
            panel3.Location = new Point(158, -2);
            panel3.Name = "panel3";
            panel3.Size = new Size(642, 72);
            panel3.TabIndex = 3;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            label3.ForeColor = SystemColors.Control;
            label3.Location = new Point(209, 14);
            label3.Name = "label3";
            label3.Size = new Size(250, 37);
            label3.TabIndex = 4;
            label3.Text = "WELCOME ADMIN";
            // 
            // panelMainDesktop
            // 
            panelMainDesktop.Location = new Point(161, 69);
            panelMainDesktop.Name = "panelMainDesktop";
            panelMainDesktop.Size = new Size(639, 382);
            panelMainDesktop.TabIndex = 4;
            // 
            // AdminDashboard
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(panelMainDesktop);
            Controls.Add(panel1);
            Controls.Add(panel3);
            FormBorderStyle = FormBorderStyle.None;
            Name = "AdminDashboard";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "AdminDashboard";
            panel1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Panel panel1;
        private Button btnMembers;
        private Button btnBooks;
        private Button btnLoans;
        private Button btnStatus;
        private Button btnAuthor;
        private Panel panel4;
        private Button btnDashboard;
        private Panel panel2;
        private Label label2;
        private Label label1;
        private Panel panel3;
        private Label label3;
        private Panel panelMainDesktop;
    }
}